package com.hrd.homework.repository;

import com.hrd.homework.model.Article;
import com.hrd.homework.model.Category;
import com.hrd.homework.model.FilterArticle;
import com.hrd.homework.model.Page;
import com.hrd.homework.provider.ArticleProvider;
import com.hrd.homework.provider.FilterProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository {

    @SelectProvider(type = ArticleProvider.class, method = "findAll")
    @Results({
            @Result(property = "url", column = "thumbnail"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "category.category_id", column = "category_id"),
            @Result(property = "category.category_name", column = "category_name"),
    })
    List<Article> findAll();

    @SelectProvider(type = ArticleProvider.class, method = "findAll")
    @Results({
            @Result(property = "url", column = "thumbnail"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "category.category_id", column = "category_id"),
            @Result(property = "category.category_name", column = "category_name"),

    })
    Article findById(int id);

    @InsertProvider(type = ArticleProvider.class, method = "add")
    @Results({
            @Result(property = "url", column = "thumbnail"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "category.category_id", column = "category_id"),
            @Result(property = "category.category_name", column = "category_name"),

    })
    void add(Article article);

    @DeleteProvider(type = ArticleProvider.class, method = "delete")
    @Results({
            @Result(property = "url", column = "thumbnail"),
            @Result(property = "createdDate", column = "created_date"),

    })
    void deleteById(int id);

    @UpdateProvider(type = ArticleProvider.class, method = "update")
    @Results({
            @Result(property = "url", column = "thumbnail"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "category.category_id", column = "category_id"),
            @Result(property = "category.category_name", column = "category_name"),

    })
    void update(Article article);

    @Select("select id from tb_article order by id DESC limit 1")
    int getLastId();

    @Select("select count(*) from tb_article")
    int getRowCount();

    @SelectProvider(type = FilterProvider.class, method = "getRowCountFilter")
    int getRowCountFilter(FilterArticle filterArticle);

    @Select("select a.id, a.title, a.description, a.thumbnail,a.created_date, a.author,c.category_id," +
            " c.category_name from tb_article a inner join tb_category c ON a.category_id = c.category_id order by id limit #{limit} offset #{row}")
    @Results({
            @Result(property = "url", column = "thumbnail"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "category.category_id", column = "category_id"),
            @Result(property = "category.category_name", column = "category_name"),

    })
    List<Article> getByPage(int limit, int row);

    @SelectProvider(type = FilterProvider.class, method = "filterByNameId")
    @Results({
            @Result(property = "url", column = "thumbnail"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "category.category_id", column = "category_id"),
            @Result(property = "category.category_name", column = "category_name"),

    })
    List<Article> filterByNameId(@Param("filter") FilterArticle article, @Param("page") Page page);

}
