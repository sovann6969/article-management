package com.hrd.homework.repository;

import com.hrd.homework.model.Article;
import com.hrd.homework.model.Category;
import com.hrd.homework.provider.CategoryProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryReposity {
    @SelectProvider(type = CategoryProvider.class, method = "getAllCategories")
    @Results({
            @Result(property = "category_name", column = "category_name"),
            @Result(property = "category_id", column = "category_id"),

    })
    List<Category> getAllCategories();

    @SelectProvider(type = CategoryProvider.class, method = "getAllCategories")
    @Results({
            @Result(property = "category_name", column = "category_name"),
            @Result(property = "category_id", column = "category_id"),

    })
    Category getCategoryByID(Integer id);

    @InsertProvider(type = CategoryProvider.class, method = "addCategory")
    @Results({
            @Result(property = "category_name", column = "category_name"),
            @Result(property = "category_id", column = "category_id"),

    })
    void addCategory(Category category);

    @UpdateProvider(type = CategoryProvider.class, method = "updateCategory")
    @Results({
            @Result(property = "category_name", column = "category_name"),
            @Result(property = "category_id", column = "category_id"),

    })
    void updateCategory(Category category);

    @Select("select category_id from tb_category order by category_id DESC limit 1")
    int getLastCategoryId();

    @DeleteProvider(type = CategoryProvider.class, method = "delCategory")
    @Results({
            @Result(property = "category_name", column = "category_name"),
            @Result(property = "category_id", column = "category_id"),

    })
    void deleteCategoryById(Integer id);
}
