package com.hrd.homework.model;

import org.springframework.lang.NonNull;

import javax.validation.constraints.NotEmpty;

public class Category {
    @NonNull
    private Integer category_id;
    @NotEmpty
    private String category_name;

    public  Category(){}
    public Category(int category_id, String category_name) {
        this.category_id = category_id;
        this.category_name = category_name;
    }

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }
}
