package com.hrd.homework.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Article {
    @NotNull
    private Integer id;
    @NotBlank
    private String title;
    @NotEmpty
    private String description;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @NotBlank
    @Size(min = 3, max = 8)
    private String author;
    private String createdDate;
    @NotEmpty
    private String url;
    private Category category;
    public Article() {

    }

    public Article(Integer id, String title, String description, String author, String createdDate) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.author = author;
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", author='" + author + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", url='" + url + '\'' +
                ", category=" + category.getCategory_name() +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Article(@NotNull Integer id, @NotBlank String title, @NotEmpty String description, @NotBlank @Size(min = 3, max = 8) String author, String createdDate, String url) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.author = author;
        this.createdDate = createdDate;
        this.url = url;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

}
