package com.hrd.homework.model;

public class FilterArticle {
    private Integer category_id;
    private String article_title;

    public FilterArticle(Integer category_id, String article_title) {
        this.category_id = category_id;
        this.article_title = article_title;
    }
    public FilterArticle(){
        this.category_id = null;
        this.article_title = null;
    }

    public Integer getCategory_id() {
        return category_id;
    }

    @Override
    public String toString() {
        return "FilterArticle{" +
                "category_id=" + category_id +
                ", article_title='" + article_title + '\'' +
                '}';
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    public String getArticle_title() {
        return article_title;
    }

    public void setArticle_title(String article_title) {
        this.article_title = article_title;
    }
}
