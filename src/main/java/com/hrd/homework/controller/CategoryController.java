package com.hrd.homework.controller;

import com.hrd.homework.model.Category;
import com.hrd.homework.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class CategoryController {
    private CategoryService categoryService;
    @Autowired
    public void setArticleService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/category")
    public String showCategory(Model model){
        model.addAttribute("categories",categoryService.getAllCategories());
        return "category/category";
    }

    @GetMapping("addCategory")
    public String addCategory(ModelMap modelMap){
        Category category = new Category();
        category.setCategory_id(categoryService.getLastCategoryId());
        modelMap.addAttribute("category", category);
        modelMap.addAttribute("isAdd", true);
        return "category/addNewCategory";
    }

    @PostMapping("addCategory")
    public String addNewCategory(@Valid @ModelAttribute Category category, BindingResult bindingResult, ModelMap modelMap){
        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("category", category);
            modelMap.addAttribute("isAdd", true);
            return "category/addNewCategory";
        }
        categoryService.addCategory(category);
        return "redirect:/category";
    }

    @GetMapping("/deleteCategory/{id}")
    public String deleteCategory(@PathVariable Integer id) {
        categoryService.deleteCategoryById(id);
        return "redirect:/category";
    }

    @GetMapping("/updateCategory/{id}")
    public String  updateCategory(ModelMap modelMap, @PathVariable Integer id){
        modelMap.addAttribute("category", categoryService.getCategoryByID(id));
        modelMap.addAttribute("isAdd", false);
        return "category/addNewCategory";
    }

    @PostMapping("/updateCategory")
    public String updateNewCategory(@ModelAttribute Category category) {
        categoryService.updateCategory(category);
        return "redirect:/category";
    }


}
