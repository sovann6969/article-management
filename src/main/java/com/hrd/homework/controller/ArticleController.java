package com.hrd.homework.controller;

import com.hrd.homework.model.Article;
import com.hrd.homework.model.FilterArticle;
import com.hrd.homework.model.Page;
import com.hrd.homework.service.ArticleService;
import com.hrd.homework.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Controller
public class ArticleController {
    @Value("${file.image.client}")
    String client;

    private CategoryService categoryService;

    @Autowired
    public void setArticleService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    private ArticleService articleService;

    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping("/")
    public String home(Model model, FilterArticle filterArticle,Page page) {

        System.out.println("filter = "+filterArticle+"pag = "+page);
//        System.out.println("rows = "+new Page().getRowCount(filterArticle));
        model.addAttribute("filter", filterArticle);
        model.addAttribute("categories", categoryService.getAllCategories());
        model.addAttribute("page",page);
        model.addAttribute("articles",articleService.filterByNameId(filterArticle,page));
//        model.addAttribute("articles", articleService.getByPage(10, 0));
//        model.addAttribute("selectedPage", 1);
//        model.addAttribute("numberOfPages", articleService.getRowCount());
        return "article/index";
    }
//
//    @GetMapping("/page/{page}")
//    public String selectedPage(Model model, @PathVariable int page, FilterArticle filterArticle) {
//        if (page > articleService.getRowCount()) {
//            return "redirect:/";
//        }
//        if (page < 1) {
//            if (page == 0) {
//                return "article/noData";
//            } else {
//                model.addAttribute("filter", filterArticle);
//                model.addAttribute("categories", categoryService.getAllCategories());
//                model.addAttribute("articles", articleService.getByPage(10, ((articleService.getRowCount() + 1) * 10) - 10));
//                model.addAttribute("numberOfPages", articleService.getRowCount());
//                model.addAttribute("selectedPage", page);
//                return "redirect:/page/" + (articleService.getRowCount());
//            }
//
//        } else {
//            model.addAttribute("filter", filterArticle);
//            model.addAttribute("categories", categoryService.getAllCategories());
//            model.addAttribute("articles", articleService.getByPage(10, (page * 10) - 10));
//            model.addAttribute("numberOfPages", articleService.getRowCount());
//            model.addAttribute("selectedPage", page);
//            return "article/index";
//        }
//
//    }

    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        Article article = new Article();
        article.setId(articleService.getLastId());
        modelMap.addAttribute("category", categoryService.getAllCategories());
        modelMap.addAttribute("article", article);
        modelMap.addAttribute("isAdd", true);
        return "article/add";
    }

    @PostMapping("/add")
    public String insertArticle(@Valid @ModelAttribute Article article, BindingResult bindingResult, ModelMap modelMap, @RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            String name = UUID.randomUUID() + file.getOriginalFilename();
            article.setUrl(name);
            try {
                Files.copy(file.getInputStream(), Paths.get(client, name));
            } catch (IOException e) {
                e.printStackTrace();
            }
            articleService.add(article);
            return "redirect:/";
        }

        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("category", categoryService.getAllCategories());
            modelMap.addAttribute("article", article);
            modelMap.addAttribute("isAdd", true);
            return "article/add";
        }
        return "article/add";
    }


    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Integer id) {
        articleService.deleteById(id);
        return "redirect:/";
    }

    @GetMapping("/update/{id}")
    public String update(ModelMap modelMap, @PathVariable Integer id) {
        modelMap.addAttribute("category", categoryService.getAllCategories());
        modelMap.addAttribute("article", articleService.findById(id));
        modelMap.addAttribute("isAdd", false);
        return "article/add";
    }

    @PostMapping("/update")
    public String updateArticle(@ModelAttribute Article article, @RequestParam("file") MultipartFile file, Model model) {
        if (!file.isEmpty()) {
            String name = UUID.randomUUID() + file.getOriginalFilename();
            article.setUrl(name);
            try {
                Files.copy(file.getInputStream(), Paths.get(client, name));
            } catch (IOException e) {
                e.printStackTrace();
            }
            articleService.update(article);
            return "redirect:/";
        }

        model.addAttribute("category", categoryService.getAllCategories());
        return "article/add";
    }

    @GetMapping("/view/{id}")
    public String view(@PathVariable Integer id, Model model) {
        Article article = articleService.findById(id);
        model.addAttribute("article", article);
        return "article/viewDetail";
    }

}
