package com.hrd.homework.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfiguration {

    @Bean
    @Profile("development")
    public DataSource development(){
        EmbeddedDatabaseBuilder embeddedDatabaseBuilder = new EmbeddedDatabaseBuilder();
        embeddedDatabaseBuilder.setType(EmbeddedDatabaseType.H2);
//        embeddedDatabaseBuilder.addDefaultScripts();
//        embeddedDatabaseBuilder.addScript("sql/tableArticle.sql");
        embeddedDatabaseBuilder.addScript("sql/data.sql");
        embeddedDatabaseBuilder.addScript("sql/insert.sql");

        return embeddedDatabaseBuilder.build();
    }

    @Bean
    @Profile("production")
    public DataSource production(){
        DriverManagerDataSource db = new DriverManagerDataSource();
        db.setUsername("postgres");
        db.setDriverClassName("org.postgresql.Driver");
        db.setUrl("jdbc:postgresql://localhost:5432/postgres");
        db.setPassword("123");
        return db;
    }
}
