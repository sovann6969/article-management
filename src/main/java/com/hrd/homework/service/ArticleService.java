package com.hrd.homework.service;

import com.hrd.homework.model.Article;
import com.hrd.homework.model.FilterArticle;
import com.hrd.homework.model.Page;

import java.util.List;

public interface ArticleService {
    List<Article> findAll();

    Article findById(int id);

    void add(Article article);

    void deleteById(int id);

    void update(Article article);

    int getLastId();

    int getRowCount();

    List<Article> getByPage(int limit, int row);

    List<Article> filterByNameId(FilterArticle article, Page page);

    int getRowCountFilter(FilterArticle filterArticle);
}
