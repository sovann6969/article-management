package com.hrd.homework.service;

import com.hrd.homework.model.Article;
import com.hrd.homework.model.FilterArticle;
import com.hrd.homework.model.Page;
import com.hrd.homework.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@Service
public class ArticleServiceImpl implements ArticleService {

    private ArticleRepository articleRepository;

    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository){
        this.articleRepository = articleRepository;
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public Article findById(int id) {
        return articleRepository.findById(id);
    }

    @Override
    public void add(Article article) {
        article.setCreatedDate(new Date().toString());
        articleRepository.add(article);
    }

    @Override
    public void deleteById(int id) {
        articleRepository.deleteById(id);
    }

    @Override
    public void update(Article article) {
        article.setCreatedDate(new Date().toString());
        articleRepository.update(article);
    }

    @Override
    public int getLastId() {
        return articleRepository.getLastId()+1;
    }

    @Override
    public int getRowCount() {
        int rows = articleRepository.getRowCount()/10;
        int a = articleRepository.getRowCount()%10;
        if(a>0){
            return Math.round(rows)+1;
        }else{
            return Math.round(rows);
        }


    }

    @Override
    public List<Article> getByPage(int limit, int row) {
        return articleRepository.getByPage(limit,row);
    }

    @Override
    public List<Article> filterByNameId(FilterArticle article, Page page) {
        page.setTotalCount(articleRepository.getRowCountFilter(article));
        return articleRepository.filterByNameId(article,page);
    }

    @Override
    public int getRowCountFilter(FilterArticle filterArticle) {
        return articleRepository.getRowCountFilter(filterArticle);
    }

}
