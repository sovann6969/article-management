package com.hrd.homework.service;

import com.hrd.homework.model.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAllCategories();

    Category getCategoryByID(Integer id);

    void addCategory(Category category);

    void updateCategory(Category category);

    int getLastCategoryId();

    void deleteCategoryById(Integer id);
}
