package com.hrd.homework.service;

import com.hrd.homework.model.Category;
import com.hrd.homework.repository.CategoryReposity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryReposity categoryReposity;

    @Autowired
    public void setCategoryService(CategoryReposity categoryReposity){
        this.categoryReposity = categoryReposity;
    }

    @Override
    public List<Category> getAllCategories() {
        return categoryReposity.getAllCategories();
    }

    @Override
    public Category getCategoryByID(Integer id) {
        return categoryReposity.getCategoryByID(id);
    }

    @Override
    public void addCategory(Category category) {
        categoryReposity.addCategory(category);
    }


    @Override
    public void updateCategory(Category category) {
        categoryReposity.updateCategory(category);
    }

    @Override
    public int getLastCategoryId() {
        return categoryReposity.getLastCategoryId()+1;
    }

    @Override
    public void deleteCategoryById(Integer id) {
        categoryReposity.deleteCategoryById(id);
    }

}
