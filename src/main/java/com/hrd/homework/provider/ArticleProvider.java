package com.hrd.homework.provider;

import com.hrd.homework.model.Article;
import com.hrd.homework.model.Category;
import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider {

    public String findAll(Integer id) {
        return new SQL() {{
            SELECT("a.id, a.title, a.description, a.thumbnail,a.created_date, a.author,c.category_id, c.category_name");
            FROM("tb_article a");
            INNER_JOIN("tb_category c ON a.category_id = c.category_id");
            if (id != null) {
                WHERE("id = " + id);
            }
            ORDER_BY("id");
        }}.toString();
    }

    public String add(Article article) {
        return new SQL() {{
            INSERT_INTO("tb_article");
            VALUES("title", "'" + article.getTitle() + "'");
            VALUES("description", "'" + article.getDescription() + "'");
            VALUES("author", "'" + article.getAuthor() + "'");
            VALUES("thumbnail", "'" + article.getUrl() + "'");
            VALUES("created_date", "'" + article.getCreatedDate() + "'");
            VALUES("category_id",""+article.getCategory().getCategory_id());
        }}.toString();
    }

    public String delete(Integer id) {
        return new SQL() {{
            DELETE_FROM("tb_article");
            WHERE("id =" + id);
        }}.toString();
    }

    public String update(Article article) {
        return new SQL() {{
            UPDATE("tb_article");
            SET("title = '"+ article.getTitle() + "'");
            SET("description = '"+ article.getDescription() + "'");
            SET("author = '"+ article.getAuthor() + "'");
            SET("thumbnail = '"+ article.getUrl() + "'");
            SET("created_date = '"+ article.getCreatedDate() + "'");
            VALUES("category_id",""+article.getCategory().getCategory_id());
            WHERE("id = '"+ article.getId() + "'");
        }}.toString();
    }


}
