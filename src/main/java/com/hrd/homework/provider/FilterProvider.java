package com.hrd.homework.provider;

import com.hrd.homework.model.Article;
import com.hrd.homework.model.Category;
import com.hrd.homework.model.FilterArticle;
import com.hrd.homework.model.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class FilterProvider {

    public String filterByNameId(@Param("filter") FilterArticle article, @Param("page") Page page) {
        return new SQL() {{
            SELECT("a.id, a.title, a.description, a.thumbnail,a.created_date, a.author,c.category_id, c.category_name");
            FROM("tb_article a");
            INNER_JOIN("tb_category c ON a.category_id = c.category_id");
            if (article.getCategory_id() != null) {
                WHERE("c.category_id = " + article.getCategory_id());
            }
            if (article.getArticle_title() != null && article.getArticle_title()!="" ) {
                WHERE("a.title ILIKE '%" + article.getArticle_title() + "%'");
            }
            ORDER_BY("a.id LIMIT #{page.limit} OFFSET #{page.offset}");
        }}.toString();
    }

    public String getRowCountFilter(FilterArticle article) {
        return new SQL() {{
            SELECT("count(*)");
            FROM(" tb_article a ");
            INNER_JOIN("tb_category c ON a.category_id = c.category_id");
            if (article.getCategory_id() != null) {
                WHERE("c.category_id = " + article.getCategory_id());
            }
            if (article.getArticle_title() != null && article.getArticle_title()!="" ) {
                WHERE("a.title ILIKE '%" + article.getArticle_title() + "%'");
            }
        }}.toString();
    }

}
