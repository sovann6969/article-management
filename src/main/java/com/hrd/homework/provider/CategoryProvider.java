package com.hrd.homework.provider;

import com.hrd.homework.model.Category;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    public String getAllCategories(Integer id){
        return new SQL(){{
            SELECT("category_id,category_name");
            FROM("tb_category");
            if(id!=null){
                WHERE("category_id =" +id);
            }
            ORDER_BY("category_id");
        }}.toString();
    }

    public String addCategory(Category category){
        return new SQL() {{
            INSERT_INTO("tb_category");
            VALUES("category_name", "'" + category.getCategory_name() + "'");
        }}.toString();
    }

    public String updateCategory(Category category) {
        return new SQL() {{
            UPDATE("tb_category");
            SET("category_name = '"+ category.getCategory_name() + "'");
        }}.toString();
    }
    public String delCategory(Integer id){
        return new SQL() {{
            DELETE_FROM("tb_category");
            WHERE("category_id =" + id);
        }}.toString();
    }
}
