
CREATE TABLE IF NOT EXISTS tb_category (
  category_id serial PRIMARY KEY NOT NULL,
  category_name varchar(50)
);

CREATE TABLE IF NOT EXISTS tb_article (
  id serial PRIMARY KEY NOT NULL,
  title varchar(50),
  description varchar(255),
  author varchar(25),
  thumbnail varchar(255),
  created_date varchar(50),
  category_id int REFERENCES tb_category (category_id) ON UPDATE CASCADE ON DELETE CASCADE
);