
CREATE TABLE tb_article(
    id int primary key auto_increment,
    title varchar (50),
    description varchar (255),
    author varchar (50),
    thumbnail varchar (100),
    created_date varchar (50)
);

CREATE TABLE tb_category(
    id int primary key auto_increment,
    name varchar (50),
);