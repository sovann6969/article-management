$(document).ready(function () {
 $('#photo').change(function (evt) {
     evt.preventDefault();
     var tgt = evt.target || window.event.srcElement,
         files = tgt.files;
     // FileReader support
     if (FileReader && files && files.length) {
         var fr = new FileReader();
         fr.onload = function () {
          var img = document.getElementById('img_show')
             img.src = fr.result;
         }
         fr.readAsDataURL(files[0]);
     }
     // Not supported
     else {
         check = false;
     }
  })
})